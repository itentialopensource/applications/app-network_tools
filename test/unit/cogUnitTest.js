// Set globals
/* global describe it log */
/* eslint global-require:warn */

// include required items for testing & logging
/* eslint-disable import/no-extraneous-dependencies */
const chai = require('chai');
const assert = require('assert');
const util = require('util');

const { expect } = chai;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const td = require('testdouble');
const { EventEmitter } = require('events');

const anything = td.matchers.anything();

let logLevel = 'none';
// const attemptTimeout = 25000;
const isRapidFail = false;

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = new (winston.Logger)({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

// require the app that we are going to be using
// eslint-disable-next-line import/extensions
//const cog = require('../../cog.js');
let cog, ipTools, dns, net;

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] NetworkTools App Test', () => {

  beforeEach(function () {
    cog = require('../../cog.js');
    dns = require('dns');
    net = require('net');
  });
  afterEach(function(){
    td.reset();
  });

  describe('NetworkTools Class Tests', () => {
    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        fs.exists('package.json', (val) => {
          assert.equal(true, val);
          done();
        });
      });
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        fs.exists('README.md', (val) => {
          assert.equal(true, val);
          log.log('README.md exists');
          done();
        });
      });
    });
  });

  describe('checkRecordExists', () => {
    it('should return an error when hostname is empty.', (done) => {
      cog.checkRecordExists('', 'A', (res, err) => {
        assert.equal(res, null);
        assert.equal(err, 'You must specify hostName as string.');
        done();
      });
    });
    it('should return an error when hostname is not string.', (done) => {
      cog.checkRecordExists({}, 'A', (res, err) => {
        assert.equal(res, null);
        assert.equal(err, 'You must specify hostName as string.');
        done();
      });
    });
    it('should return an error when recordType is empty string.', (done) => {
      cog.checkRecordExists('google.com', '', (res, err) => {
        assert.equal(res, null);
        assert.equal(err, 'You must specify record type as string.');
        done();
      });
    });
    it('should return an error when recordType is null.', (done) => {
      cog.checkRecordExists('google.com', null, (res, err) => {
        assert.equal(res, null);
        assert.equal(err, 'You must specify record type as string.');
        done();
      });
    });
    it('should return an error when recordType is not string.', (done) => {
      cog.checkRecordExists('google.com', {}, (res, err) => {
        assert.equal(res, null);
        assert.equal(err, 'You must specify record type as string.');
        done();
      });
    });
    it('should return an error when recordType is ANY.', (done) => {
      cog.checkRecordExists('google.com', 'ANY', (res, err) => {
        assert.equal(res, null);
        assert.equal(err, 'ANY record type check is not yet supported.');
        done();
      });
    });
    it('should return false when DNS ENODATA error.', (done) => {
      td.replace(dns, 'resolve');
      td.when(dns.resolve(anything, anything)).thenCallback({ code: 'ENODATA' }, null);
      cog.checkRecordExists('google.com', 'A', (res, err) => {
        assert.equal(res, false);
        assert.equal(err, null);
        done();
      });
    });
    it('should return error message when dns error.', (done) => {
      const errMsg = 'my error message';
      td.replace(dns, 'resolve');
      td.when(dns.resolve(anything, anything)).thenCallback(errMsg, null);
      cog.checkRecordExists('google.com', 'A', (res, err) => {
        assert.equal(res, null);
        assert.equal(err, errMsg);
        done();
      });
    });
    it('should return true when records found.', (done) => {
      td.replace(dns, 'resolve');
      td.when(dns.resolve(anything, anything)).thenCallback(null, { 1: '1', 2: '2' });
      cog.checkRecordExists('google.com', 'A', (res, err) => {
        assert.equal(res, true);
        assert.equal(err, null);
        done();
      });
    });
    it('should return false when no records found.', (done) => {
      td.replace(dns, 'resolve');
      td.when(dns.resolve(anything, anything)).thenCallback(null, {});
      cog.checkRecordExists('google.com', 'A', (res, err) => {
        assert.equal(res, false);
        assert.equal(err, null);
        done();
      });
    });
  });

  describe('checkPTRRecordExists', () => {
    it('should return error when ipAddress is not type string.', (done) => {
      cog.checkPTRRecordExists({}, (data, err) => {
        assert.equal(data, null);
        assert.equal(err, 'You must specify ipAddress as string.');
        done();
      });
    });
    it('should return false when dns returns ENOTFOUND error.', (done) => {
      td.replace(dns, 'reverse');
      td.when(dns.reverse('1.2.3.4')).thenCallback(null, { code: 'ENOTFOUND' });
      cog.checkPTRRecordExists('1.2.3.4', (data, err) => {
        assert.equal(data, false);
        assert.equal(err, null);
        done();
      });
    });
    it('should return error message on every other error.', (done) => {
      const errMsg = { message: 'my error code' };
      td.replace(dns, 'reverse');
      td.when(dns.reverse(anything)).thenCallback(errMsg, null);
      cog.checkPTRRecordExists('1.2.3.4', (data, err) => {
        assert.equal(data, null);
        assert.equal(err, errMsg);
        done();
      });
    });
    it('should return false if no record found.', (done) => {
      td.replace(dns, 'reverse');
      td.when(dns.reverse(anything)).thenCallback(null, []);
      cog.checkPTRRecordExists('1.2.3.4', (data, err) => {
        assert.equal(data, false);
        assert.equal(err, null);
        done();
      });
    });
    it('should return true if any record found.', (done) => {
      td.replace(dns, 'reverse');
      td.when(dns.reverse(anything)).thenCallback(null, [1, 2, 3]);
      cog.checkPTRRecordExists('1.2.3.4', (data, err) => {
        assert.equal(data, true);
        assert.equal(err, null);
        done();
      });
    });
  });

  describe('checkPortConnection', () => {
    describe('verify inputs', () => {
      it('should exit with an error when hostname is an empty string.', (done) => {
        cog.checkPortConnection('', 80, 5, 5000, (data, err) => {
          assert.equal(data, null);
          assert.equal(err, 'You must specify hostName as a string.');
          done();
        });
      });
      it('should exit with an error when hostname is not a string.', (done) => {
        cog.checkPortConnection({}, 80, 5, 5000, (data, err) => {
          assert.equal(data, null);
          assert.equal(err, 'You must specify hostName as a string.');
          done();
        });
      });
      it('should exit with an error when port is null.', (done) => {
        cog.checkPortConnection('8.8.8.8', null, 5, 5000, (data, err) => {
          assert.equal(data, null);
          assert.equal(err, 'You must specify port as a number.');
          done();
        });
      });
      it('should exit with an error when port is not a number.', (done) => {
        cog.checkPortConnection('8.8.8.8', '80', 5, 5000, (data, err) => {
          assert.equal(data, null);
          assert.equal(err, 'You must specify port as a number.');
          done();
        });
      });
      it('should exit with an error when retries is not a number.', (done) => {
        cog.checkPortConnection('8.8.8.8', 80, '5', 5000, (data, err) => {
          assert.equal(data, null);
          assert.equal(err, 'You must specify retries as a positive integer greater than 0.');
          done();
        });
      });
      it('should exit with an error when timeout is not a number.', (done) => {
        cog.checkPortConnection('8.8.8.8', 80, 5, '5000', (data, err) => {
          assert.equal(data, null);
          assert.equal(err, 'You must specify timeout as a number greater than 0 and less than 10000.');
          done();
        });
      });
      it('should exit with an error when timeout is not a positive number.', (done) => {
        cog.checkPortConnection('8.8.8.8', 80, 5, -1000, (data, err) => {
          assert.equal(data, null);
          assert.equal(err, 'You must specify timeout as a number greater than 0 and less than 10000.');
          done();
        });
      });
      it('should exit with an error when timeout is greater than 10s.', (done) => {
        cog.checkPortConnection('8.8.8.8', 80, 5, 20000, (data, err) => {
          assert.equal(data, null);
          assert.equal(err, 'You must specify timeout as a number greater than 0 and less than 10000.');
          done();
        });
      });
    });
    describe('test tryConnect', () => {
      let client;

      // create an event listener and attach the methods to stub in order to fire mock events
      const createClient = () => {
        client = new EventEmitter();
        client.connect = td.func();
        client.setTimeout = td.func();
        client.destroy = td.func();
        return client;
      };

      mocha.beforeEach(() => {
        td.replace(net, 'Socket');
        // create a new "client" each time it is asked for by tryConnect function
        td.when(new net.Socket()).thenReturn(createClient());
        // destroy all attached event listeners to mock the Socket.prototype.destroy method
        td.when(client.destroy()).thenDo(() => client.removeAllListeners());
      });

      it('should return true on successful connection.', (done) => {
        td.when(client.connect(anything, anything)).thenDo(() => client.emit('connect'));
        cog.checkPortConnection('hostname', 80, 5, 5000, (data, err) => {
          assert.equal(data, true);
          assert.equal(err, null);
          done();
        });
      });
      it('should return false on connection timeout.', (done) => {
        td.when(client.setTimeout(anything)).thenDo(() => client.emit('timeout'));
        cog.checkPortConnection('hostname', 80, 5, 5000, (data, err) => {
          assert.equal(data, false);
          assert.equal(err, null);
          done();
        });
      });
      it('should return error on connection error.', (done) => {
        td.when(client.connect(anything, anything)).thenDo(() => client.emit('error'));
        cog.checkPortConnection('hostname', 80, 5, 5000, (data) => {
          assert.equal(data, false);
          done();
        });
      });
    });
  });

  describe('checkPortConnectionList', () => {
    describe('verify inputs', () => {
      it('should exit with an error when deviceArray is null', (done) => {
        cog.checkPortConnectionList(null, 5, 5000, (data, err) => {
          assert.equal(data, null);
          assert.equal(err, 'checkPortConnectionList: deviceArray must be an array');
          done();
        });
      });
      it('should exit with an error when deviceArray is not an array', (done) => {
        cog.checkPortConnectionList('', 5, 5000, (data, err) => {
          assert.equal(data, null);
          assert.equal(err, 'checkPortConnectionList: deviceArray must be an array');
          done();
        });
      });
    });
  });

  describe('parseUrl', () => {
    it('should exit with an error when urlString is null', (done) => {
      cog.parseUrl(null, (data, err) => {
        assert.equal(data, null);
        assert.equal(err, 'You must specify urlString as string.');
        done();
      });
    });
    it('should exit with an error when urlString is not a string', (done) => {
      cog.parseUrl([], (data, err) => {
        assert.equal(data, null);
        assert.equal(err, 'You must specify urlString as string.');
        done();
      });
    });
    it('should exit with an error when urlString is invalid', (done) => {
      cog.parseUrl('evilsite.googledrive.comevilsite', (data, err) => {
        assert.equal(data, null);
        expect(err).to.be.an.instanceof(TypeError);
        expect(() => { cog.parseUrl('evilsite.googledrive.comevilsite'); }).to.throw(TypeError);
        done();
      });
    });
    it('Example 1: should return URL properties when valid URL string is passed in found.', (done) => {
      cog.parseUrl('http://example.com', (data, err) => {
        const output = {
          href: 'http://example.com/',
          origin: 'http://example.com',
          protocol: 'http:',
          username: '',
          host: 'example.com',
          hostname: 'example.com',
          port: '',
          pathname: '/',
          search: '',
          hash: ''
        };
        assert.equal(err, null);
        assert.deepEqual(data, output);
        done();
      });
    });
    it('Example 2: should return URL properties when valid URL string is passed in found.', (done) => {
      cog.parseUrl('https://www.verybadsite.com/spglobal.com/login.html', (data, err) => {
        const output = {
          hash: '',
          host: 'www.verybadsite.com',
          hostname: 'www.verybadsite.com',
          href: 'https://www.verybadsite.com/spglobal.com/login.html',
          origin: 'https://www.verybadsite.com',
          pathname: '/spglobal.com/login.html',
          port: '',
          protocol: 'https:',
          search: '',
          username: ''
        };
        assert.equal(err, null);
        assert.deepEqual(data, output);
        done();
      });
    });
    it('Example 3: should return URL properties when valid URL string is passed in found.', (done) => {
      cog.parseUrl('https://badsite.goodsite.co.uk/ihsm/fakelogin.html', (data, err) => {
        const output = {
          hash: '',
          host: 'badsite.goodsite.co.uk',
          hostname: 'badsite.goodsite.co.uk',
          href: 'https://badsite.goodsite.co.uk/ihsm/fakelogin.html',
          origin: 'https://badsite.goodsite.co.uk',
          pathname: '/ihsm/fakelogin.html',
          port: '',
          protocol: 'https:',
          search: '',
          username: ''
        };
        assert.equal(err, null);
        assert.deepEqual(data, output);
        done();
      });
    });
    it('Example 4: should exit with an error when urlString is invalid', (done) => {
      cog.parseUrl('Spglobal.com.0nedrive.com.secureaccess-badsite.com/login.html', (data, err) => {
        assert.equal(data, null);
        expect(err).to.be.an.instanceof(TypeError);
        expect(() => { cog.parseUrl('Spglobal.com.0nedrive.com.secureaccess-badsite.com/login.html'); }).to.throw(TypeError);
        done();
      });
    });
  });

  describe('getIPAddresOfCurrentIAPServer', () => {
    it('should return an IP address.', (done) => {
      cog.getIPAddresOfCurrentIAPServer((res, err) => {
        expect(res).not.to.equal(null);
        expect(res).not.to.equal(undefined);
        expect(err).to.equal(null);
        done();
      });
    });
    it('should return and error.', (done) => {
      const osreplace = td.replace('os');
      const cogtmp = require('../../cog');
      td.when(osreplace.networkInterfaces()).thenReturn(null);
      cogtmp.getIPAddresOfCurrentIAPServer((res, err) => {
        expect(err).not.to.equal(null);
        expect(res).to.equal(null);
        done();
      });
    });
  });

  describe('printEnvVariable', () => {
    it('should exit with an error when envVariable is null', (done) => {
      cog.printEnvVariable(null, (data, err) => {
        assert.equal(data, null);
        assert.equal(err, 'You must specify envVariable as string.');
        done();
      });
    });
    it('should return HOME env variable', (done) => {
      cog.printEnvVariable('HOME', (data, err) => {
        expect(err).to.equal(null);
        expect(data).not.to.equal(null);
        expect(data).not.to.equal(undefined);
        done();
      });
    });
    it('should return an undefined result', (done) => {
      cog.printEnvVariable('HDsdfsdgfdshFH', (data, err) => {
        expect(err).to.equal(null);
        expect(data).to.equal(undefined);
        done();
      });
    });
  });

});
