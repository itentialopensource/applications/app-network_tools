## Members

<dl>
<dt><a href="#pronghornTitleBlock">pronghornTitleBlock</a> : <code>Application</code></dt>
<dd><p>This is the app/interface into Dig</p>
</dd>
<dt><a href="#resolveHostName">resolveHostName</a> ⇒ <code>object</code></dt>
<dd><p>Ask a dns/dig question to find records using host name.</p>
</dd>
<dt><a href="#checkRecordExists">checkRecordExists</a> ⇒ <code>object</code></dt>
<dd><p>Does record lookup and returns a boolean</p>
</dd>
<dt><a href="#reverseIpAddr">reverseIpAddr</a> ⇒ <code>object</code></dt>
<dd><p>Ask a dns/dig question to find PTR record using IP address.</p>
</dd>
<dt><a href="#checkPTRRecordExists">checkPTRRecordExists</a> ⇒ <code>object</code></dt>
<dd><p>Does reverse lookup and returns a boolean</p>
</dd>
<dt><a href="#checkPortConnection">checkPortConnection</a> ⇒ <code>object</code></dt>
<dd><p>Does port connection check and returns a boolean.</p>
</dd>
<dt><a href="#checkPortConnectionList">checkPortConnectionList</a> ⇒ <code>array</code></dt>
<dd><p>Synchronously does port connection check and returns a boolean</p>
</dd>
<dt><a href="#parseUrl">parseUrl</a> ⇒ <code>object</code></dt>
<dd><p>This method takes a URL string, parses it, and it will return
a URL object with each part of the address as properties</p>
</dd>
<dt><a href="#getIPAddresOfCurrentIAPServer">getIPAddresOfCurrentIAPServer</a> ⇒ <code>object</code></dt>
<dd><p>This method gets the IP address of the IAP server the task is running on</p>
</dd>
<dt><a href="#printEnvVariable">printEnvVariable</a> ⇒ <code>object</code></dt>
<dd><p>This method prints a given ENV variable</p>
</dd>
</dl>

<a name="pronghornTitleBlock"></a>

## pronghornTitleBlock : <code>Application</code>
This is the app/interface into Dig

**Kind**: global variable  
**Summary**: Basic network operations, including a Node.js DNS wrapper  
**Pronghornid**: @itentialopensource/app-network_tools  
**Title**: network_tools  
**Displayname**: Network Tools  
**Src**: cog.js  
**Encrypted**: false  
**Roles**: admin  
<a name="resolveHostName"></a>

## resolveHostName ⇒ <code>object</code>
Ask a dns/dig question to find records using host name.

**Kind**: global variable  
**Summary**: Find records using host name  
**Returns**: <code>object</code> - response Response from dig  
**Pronghorntype**: method  
**Variabletooltip**: <code>hostName</code> e.g. google.com  
**Variabletooltip**: <code>recordType</code> e.g. A or /A/  
**Variabletooltip**: <code>getLastRecord</code> Get last object returned only?  
**Task**: true  
**Roles**: admin  
**Route**: <code>POST</code> /resolveHostName  

| Param | Type | Description |
| --- | --- | --- |
| hostName | <code>string</code> | e.g. google.com |
| recordType | <code>string</code> | e.g. A or /A/ |
| getLastRecord | <code>boolean</code> | return only the last record object found by dig. |
| callback |  |  |

**Example**  
```js
{
   "hostName": "google.com",
   "recordType": "A",
   "getLastRecord": true
}
```
<a name="checkRecordExists"></a>

## checkRecordExists ⇒ <code>object</code>
Does record lookup and returns a boolean

**Kind**: global variable  
**Summary**: Check if a record with the same name exists  
**Returns**: <code>object</code> - response Response from dig  
**Pronghorntype**: method  
**Variabletooltip**: <code>hostName</code> e.g. google.com  
**Variabletooltip**: <code>recordType</code> e.g. A or /A/  
**Route**: <code>GET</code> /checkRecordExists/:hostName/:recordType  
**Roles**: admin  
**Task**: true  

| Param | Type | Description |
| --- | --- | --- |
| hostName | <code>string</code> | e.g. google.com |
| recordType | <code>string</code> | e.g. A or /A/ |
| callback |  |  |

**Example**  
```js
{
   "hostName": "google.com",
   "recordType": "A"
}
```
<a name="reverseIpAddr"></a>

## reverseIpAddr ⇒ <code>object</code>
Ask a dns/dig question to find PTR record using IP address.

**Kind**: global variable  
**Summary**: Find PTR record using IP address  
**Returns**: <code>object</code> - response Response from dig  
**Pronghorntype**: method  
**Variabletooltip**: <code>ipAddress</code> e.g. 8.8.8.4  
**Variabletooltip**: <code>getLastRecord</code> Get last object returned only?  
**Roles**: admin  
**Task**: true  
**Route**: <code>POST</code> /reverseIpAddr  

| Param | Type | Description |
| --- | --- | --- |
| ipAddress | <code>string</code> | e.g. 8.8.8.4 |
| getLastRecord | <code>boolean</code> | return only the last record object found by dig. |
| callback |  |  |

**Example**  
```js
{
   "ipAddress": "8.8.8.4",
   "getLastRecord": true
}
```
<a name="checkPTRRecordExists"></a>

## checkPTRRecordExists ⇒ <code>object</code>
Does reverse lookup and returns a boolean

**Kind**: global variable  
**Summary**: Check if the PTR record exists  
**Returns**: <code>object</code> - response Response from dig  
**Pronghorntype**: method  
**Variabletooltip**: <code>ipAddress</code> e.g. 8.8.8.4  
**Route**: <code>GET</code> /checkPTRRecordExists/:ipAddress  
**Roles**: admin  
**Task**: true  

| Param | Type | Description |
| --- | --- | --- |
| ipAddress | <code>string</code> | e.g. 8.8.8.4 |
| callback |  |  |

**Example**  
```js
{
   "ipAddress": "8.8.8.4"
}
```
<a name="checkPortConnection"></a>

## checkPortConnection ⇒ <code>object</code>
Does port connection check and returns a boolean.

**Kind**: global variable  
**Summary**: Check if connection to host on port exists, with retries  
**Returns**: <code>object</code> - portConnection Result port connection test  
**Pronghorntype**: method  
**Variabletooltip**: <code>hostName</code> e.g. google.com  
**Variabletooltip**: <code>port</code> e.g. 80  
**Variabletooltip**: <code>retries</code> e.g. 5  
**Variabletooltip**: <code>timeout</code> e.g. 5000  
**Route**: <code>POST</code> /checkPortConnection  
**Roles**: admin  
**Task**: true  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| hostName | <code>string</code> |  | e.g. google.com |
| port | <code>number</code> |  | e.g. 80 |
| [retries] | <code>number</code> | <code>5</code> |  |
| [timeout] | <code>number</code> | <code>1000</code> |  |
| callback |  |  |  |

**Example**  
```js
{
   "hostName": "google.com",
   "port": 80,
   "retries": 5,
   "timeout": 1000
}
```
<a name="checkPortConnectionList"></a>

## checkPortConnectionList ⇒ <code>array</code>
Synchronously does port connection check and returns a boolean

**Kind**: global variable  
**Summary**: Synchronously check if connection to hosts on ports exist, with retries  
**Returns**: <code>array</code> - portConnectionList Result port connection test  
**Pronghorntype**: method  
**Variabletooltip**: <code>deviceArray</code> e.g. Array of objects containing name and port.  
**Variabletooltip**: <code>retries</code> e.g. 5  
**Variabletooltip**: <code>timeout</code> e.g. 5000  
**Route**: <code>POST</code> /checkPortConnectionList  
**Roles**: admin  
**Task**: true  

| Param | Type | Default |
| --- | --- | --- |
| deviceArray | <code>array</code> |  | 
| [retries] | <code>number</code> | <code>1</code> | 
| [timeout] | <code>number</code> | <code>1000</code> | 
| callback |  |  | 

**Example**  
```js
{
   "deviceArray": [{ "name": "google.com", "port": 80 }],
   "retries": 5,
   "timeout": 5000
}
```
<a name="parseUrl"></a>

## parseUrl ⇒ <code>object</code>
This method takes a URL string, parses it, and it will return
a URL object with each part of the address as properties

**Kind**: global variable  
**Summary**: Parse URL string to return different parts of URL  
**Returns**: <code>object</code> - response Parsed URL object  
**Pronghorntype**: method  
**Variabletooltip**: <code>urlString</code> e.g. http://example.com  
**Roles**: admin  
**Task**: true  
**Route**: <code>POST</code> /parseUrl  

| Param | Type | Description |
| --- | --- | --- |
| urlString | <code>string</code> | e.g. http://example.com |
| callback |  |  |

**Example**  
```js
{
   "urlString": "http://example.com"
}
```
<a name="getIPAddresOfCurrentIAPServer"></a>

## getIPAddresOfCurrentIAPServer ⇒ <code>object</code>
This method gets the IP address of the IAP server the task is running on

**Kind**: global variable  
**Summary**: Gets the IP address of the IAP server the task is running on  
**Returns**: <code>object</code> - IP addresss of the IAP server the task is running on  
**Pronghorntype**: method  
**Roles**: admin  
**Task**: true  
**Route**: <code>GET</code> /getIPAddresOfCurrentIAPServer  

| Param |
| --- |
| callback | 

<a name="printEnvVariable"></a>

## printEnvVariable ⇒ <code>object</code>
This method prints a given ENV variable

**Kind**: global variable  
**Summary**: printenv  
**Returns**: <code>object</code> - response on printenv  
**Pronghorntype**: method  
**Roles**: admin  
**Task**: true  
**Route**: <code>POST</code> /printEnvVariable  

| Param | Type |
| --- | --- |
| envVariable | <code>string</code> | 
| callback |  | 

