# app-network_tools

An application to be used in Itential Automation Platform providing basic network operations, including a wrapper for dig tools using the [NodeJS DNS module](https://nodejs.org/api/dns.html).

Allows DNS/dig commands to be used with Workflow Builder.

  - [How to Install](#how-to-install)
  - [How to Run](#how-to-run)
  - [Available tasks](#available-tasks)
    - [resolveHostName](#resolvehostname)
    - [checkRecordExists](#checkrecordexists)
    - [reverseIpAddr](#reverseipaddr)
    - [checkPTRRecordExists](#checkptrrecordexists)
    - [checkPortConnection](#checkPortConnection)
    - [getIPAddresOfCurrentIAPServer](#getIPAddresOfCurrentIAPServer)
    - [printEnvVariable](#printEnvVariable)
    - [parseUrl](#parseUrl)
    - [validateAddresses](#validateAddresses)
    - [addressProperties](#addressProperties)
    - [cidrContains](#cidrContains)
    - [cidrOverlaps](#cidrOverlaps)
    - [mergeNetworks](#mergeNetworks)
    - [excludeNetwork](#excludeNetwork)
    - [normalizeIPAddresses](#normalizeIPAddresses)
  - [Sample Workflow](#sample-workflow)
  - [Contributing to app-network_tools](#contributing-to-app-network_tools)
  - [License & Maintainers](#license--maintainers)
    - [Product License](#product-license)


## How to Install

In order to install app-network_tools, please use either of the following methods:

- git
  - git clone this repo using the `git clone git@gitlab.com:itentialopensource/applications/app-network_tools.git` command
  - cd onto the repo folder and run `npm i` to install all dependencies
  - place the app in the appropriate folder within your IAP installation (`node_modules/@itentialopensource/app-network_tools`)
- npm
  - use the following npm command to install app onto IAP: `npm i @itentialopensource/app-network_tools`
  - verify that app is located in the appropriate folder within IAP (`node_modules/@itentialopensource/app-network_tools`)

To complete the installation, please restart IAP.
Verify that app is running using the setting or admin_essentials view

## How to Run

After verifying that App is running, navigate to workflow builder/automation studio, and build a new flow. On the right sidebar you will find App Network Tools cog and all its avialable tasks.

## Available tasks

### resolveHostName

- `resolveHostName` performs DNS Lookup on one or 'ANY' DNS Record type and returns an array of record objects.
  The function wraps [dns.resolve](https://nodejs.org/api/dns.html#dns_dns_resolve_hostname_rrtype_callback).
  Provide a hostname (e.g. google.com) and a record type (e.g. A), the task will return an array of objects or throw a DNS error. The getLastRecord option will pop the last item in the array and return an object.

![resolveHostName](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/resolveHostName.png)

### checkRecordExists

- `checkRecordExists` performs DNS Lookup on one DNS Record type and returns a boolean true if the record exists. Use this task with an evaluation (Eval) task.
  The function wraps [dns.resolve](https://nodejs.org/api/dns.html#dns_dns_resolve_hostname_rrtype_callback).
  Provide a hostname (e.g. google.com) and a record type (e.g. A), the task will return a boolean.

![checkRecordExists](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/checkRecordExists.png)

### reverseIpAddr

- `reverseIpAddr` performs Reverse DNS Lookup on only PTR records and returns an array of record objects.
  The function wraps [dns.reverse](https://nodejs.org/api/dns.html#dns_dns_reverse_ip_callback).
  Provide an IP Address (e.g. 8.8.8.8), the task will return an array of objects or throw a DNS error. The getLastRecord option will pop the last item in the array and return an object.

![reverseIpAddr](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/reverseIpAddr.png)

### checkPTRRecordExists

- `checkPTRRecordExists` performs Reverse DNS Lookup on only PTR records and returns a boolean true if the record exists. Use this task with an evaluation (Eval) task.
  The function wraps [dns.reverse](https://nodejs.org/api/dns.html#dns_dns_reverse_ip_callback).
  Provide an IP Address (e.g. 8.8.8.8), the task will return a boolean.

![checkPTRRecordExists](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/checkPTRRecordExists.png)

### checkPortConnection

- `checkPortConnection` checks the connection to a hostname on a port, and performs retries if necessary. Returns true on connection success, and returns false on timeout or error.

### getIPAddresOfCurrentIAPServer

- `getIPAddresOfCurrentIAPServer` Returns the IP address of the IAP server the task is running on.

### printEnvVariable

- `printEnvVariable` Returns the value of a given ENV variable from the IAP server.

### parseUrl

* `parseUrl` Returns an object that represents all the parts of a URL. An elegant and Itential friendly wrapper for https://nodejs.org/api/url.html

### validateAddresses

* `validateAddresses` Validates a list of IP addresses and returns an array of objects for each indicating whether or not the address is valid or not. Supports IPv4 and IPv6 and CIDR notation.

### addressProperties

* `addressProperties` Validates a list of IP addresses and returns an array of objects for each containing all discernable properties. Supports IPv4 and IPv6 and CIDR notation.

### cidrContains

* `cidrContains` Given a CIDR address and an IP address this method will confirm if the IP address is contained within the CIDR.

### cidrOverlaps

* `cidrOverlaps` Given a pair of CIDR addresses this method will confirm if they overlap.

### mergeNetworks

* `mergeNetworks` Given two contiguous IPs or CIDRs this method will merge them into 1 and return the new CIDR. If they are not merge-able it will return both networks.

### excludeNetwork

* `excludeNetwork` This method will return a new CIDR that no longer includes the given IP. 

### normalizeIPAddresses

* `normalizeIPAddresses` Performs some basic normalization on IPv4 and IPv6 addresses. For IPv4 it will normalize a /32 CIDR to an address.  For IPv6 it will expand all zero blocks and any elided blocks.

## Sample Workflow

A sample automation is available under the workflows folder of this repo (simply import the JSON file onto automation studio):

`IAP Artifacts Network Tools - Test` allows users to test the functionality of all available tasks of app-network_tools using customized values for the `hostName`, `ipAddress`, `recordType`, `port`, `retries`, and `timeout` variables.

![app-network_tools Test workflow](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/sampleworkflow.png)

Click the Play button (&#9658;) on the Workflow Builder page to start the workflow.

![app-network_tools Test start workflow](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/startworkflow.png)

Click "Start" after entering Job Description information.

![app-network_tools Test job description](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/jobdescription.png)

Replace "null" with desired values, with strings in quotes, and click "Start."

![app-network_tools Test variables](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/workflowvariables.png)

Once the automated tasks are complete, click "Work Task" on the Details page for the workflow to view the report showing the inputs and outputs for each of the available tasks.

![app-network_tools Test workflow report](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/report.png)

## Contributing to app-network_tools

Please check out the [Contributing Guidelines](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/CONTRIBUTING.md).

## License & Maintainers

### [Maintainers](./CODEOWNERS)

### Product License

[Apache 2.0](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/LICENSE)
