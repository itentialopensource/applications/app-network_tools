module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true
  },
  extends: 'airbnb-base',
  plugins: [
    'json'
  ],
  parserOptions: {
    sourceType: 'module'
  },
  rules: {
    'max-len': 'warn',
    'comma-dangle': ['error', 'never'],
    'no-unused-vars': 'warn',
    'quote-props': ['error', 'consistent'],
    'no-underscore-dangle': ['error', { 'allow': ['_addr'] }],
    'prefer-destructuring': 'warn',
    'no-param-reassign': 'warn'
  }
};
