/* eslint-disable class-methods-use-this */
/* @copyright Itential, LLC 2019 */

/* eslint-disable default-param-last */
/* eslint import/no-dynamic-require: warn */
/* eslint object-curly-newline: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint camelcase: warn  */
/* eslint-disable no-trailing-spaces */

// Set globals
/* global log */

/* Required libraries.  */
const util = require('util');
const path = require('path');
const dns = require('dns');
const net = require('net');
const { networkInterfaces } = require('os');
const itentialPromisify = require('@itentialopensource/itentialpromisify');

/**
 * This is the app/interface into Dig
 *
 * @name pronghornTitleBlock
 * @pronghornId @itentialopensource/app-network_tools
 * @title network_tools
 * @export NetworkTools
 * @type Application
 * @summary Basic network operations, including a Node.js DNS wrapper
 * @displayName Network Tools
 * @src cog.js
 * @encrypted false
 * @roles admin
 */

class NetworkTools {
  constructor() {
    this.checkPortConnectionList = this.checkPortConnectionList.bind(this);
  }

  /**
   * Retrieves records associated with a given host name
   *
   * @description Ask a dns/dig question to find records using host name.
   * @pronghornType method
   * @name resolveHostName
   * @summary Find records using host name
   * @param {string} hostName - e.g. google.com
   * @variableTooltip {hostName} e.g. google.com
   * @param {string} recordType - e.g. A or /A/
   * @variableTooltip {recordType} e.g. A or /A/
   * @param {boolean} getLastRecord - return only the last record object found by dig.
   * @variableTooltip {getLastRecord} Get last object returned only?
   * @param callback
   * @return {object} response Response from dig
   * @task true
   * @roles admin
   * @route {POST} /resolveHostName
   * @example
   * {
   *    "hostName": "google.com",
   *    "recordType": "A",
   *    "getLastRecord": true
   * }
   */
  resolveHostName(hostName, recordType = 'ANY', getLastRecord = false, callback) {
    if (typeof hostName !== 'string' || hostName === '') {
      return callback(null, 'You must specify hostName as string.');
    }
    try {
      log.debug(`In resolveHostName, with ${hostName}, ${recordType}, ${getLastRecord}`);
      return dns.resolve(hostName, recordType, (error, addresses) => {
        if (error) {
          log.error(error);
          return callback(null, error);
        }
        let answer = addresses;
        // If they pass the getLastRecord flag, pop the last answer from the array.
        if (answer && getLastRecord) {
          Object.keys(answer).forEach((key) => {
            answer = answer[key] ? answer[key] : '';
          });
        }
        return callback(answer, null);
      });
    } catch (err) {
      return callback(null, err);
    }
  }

  /**
   * Returns a boolean true if there's a record associated with the host name
   *
   * @description Does record lookup and returns a boolean
   * @pronghornType method
   * @name checkRecordExists
   * @summary Check if a record with the same name exists
   * @param {string} hostName - e.g. google.com
   * @variableTooltip {hostName} e.g. google.com
   * @param {string} recordType - e.g. A or /A/
   * @variableTooltip {recordType} e.g. A or /A/
   * @param callback
   * @return {object} response Response from dig
   * @route {GET} /checkRecordExists/:hostName/:recordType
   * @roles admin
   * @task true
   * @example
   * {
   *    "hostName": "google.com",
   *    "recordType": "A"
   * }
   */
  checkRecordExists(hostName, recordType, callback) {
    if (typeof hostName !== 'string' || hostName === '') {
      return callback(null, 'You must specify hostName as string.');
    }
    if (recordType === '' || !recordType || typeof recordType !== 'string') {
      return callback(null, 'You must specify record type as string.');
    }
    if (recordType === 'ANY') {
      return callback(null, 'ANY record type check is not yet supported.');
    }
    // Todo: Add checks for supported Record Types
    try {
      return this.resolveHostName(hostName, recordType, false, (data, err) => {
        // if the error is a DNS ENODATA error, then PTR record does not exist. Return false.
        if (err && (err.code === 'ENODATA' || err.code === 'ENOTFOUND')) {
          return callback(false, null);
        }
        // if it's some other error, we need to notify the user
        if (err) {
          return callback(null, err);
        }
        // Check the response length
        const response = data && Object.keys(data) ? Object.keys(data).length > 0 : false;
        return callback(response, null);
      });
    } catch (err) {
      return callback(null, err);
    }
  }

  /**
   * Does DNS reverse lookup on the specified IP address. Returns the associated PTR host name.
   *
   * @description Ask a dns/dig question to find PTR record using IP address.
   * @pronghornType method
   * @name reverseIpAddr
   * @summary Find PTR record using IP address
   * @param {string} ipAddress - e.g. 8.8.8.4
   * @variableTooltip {ipAddress} e.g. 8.8.8.4
   * @param {boolean} getLastRecord - return only the last record object found by dig.
   * @variableTooltip {getLastRecord} Get last object returned only?
   * @param callback
   * @return {object} response Response from dig
   * @roles admin
   * @task true
   * @route {POST} /reverseIpAddr
   * @example
   * {
   *    "ipAddress": "8.8.8.4",
   *    "getLastRecord": true
   * }
   */
  reverseIpAddr(ipAddress, getLastRecord = false, callback) {
    if (typeof ipAddress !== 'string' || ipAddress === '') {
      return callback(null, 'You must specify ipAddress as string.');
    }
    try {
      return dns.reverse(ipAddress, (error, hostName) => {
        if (error) {
          return callback(null, error);
        }
        let answer = hostName;
        // If they pass the getLastRecord flag, pop the last answer from the array.
        if (answer && getLastRecord) {
          answer = answer.pop();
        }
        return callback(answer, null);
      });
    } catch (err) {
      return callback(null, err);
    }
  }

  /**
   * Checks for the existence of a PTR record by doing a DNS reverse lookup.
   * Returns a boolean true if record exists.
   *
   * @description Does reverse lookup and returns a boolean
   * @pronghornType method
   * @name checkPTRRecordExists
   * @summary Check if the PTR record exists
   * @param {string} ipAddress - e.g. 8.8.8.4
   * @variableTooltip {ipAddress} e.g. 8.8.8.4
   * @param callback
   * @return {object} response Response from dig
   * @route {GET} /checkPTRRecordExists/:ipAddress
   * @roles admin
   * @task true
   * @example
   * {
   *    "ipAddress": "8.8.8.4"
   * }
   */
  checkPTRRecordExists(ipAddress, callback) {
    if (typeof ipAddress !== 'string' || ipAddress === '') {
      return callback(null, 'You must specify ipAddress as string.');
    }
    try {
      return this.reverseIpAddr(ipAddress, false, (data, err) => {
        // if the error is a DNS ENOTFOUND error, then PTR record does not exist. Return false.
        if (err && (err.code === 'ENODATA' || err.code === 'ENOTFOUND')) {
          return callback(false, null);
        }
        // if it's some other error, we need to notify the user
        if (err) {
          log.error(JSON.stringify(err));
          return callback(null, err);
        }
        // Check the response length
        const response = data ? data.length > 0 : false;
        return callback(response, null);
      });
    } catch (err) {
      return callback(null, err);
    }
  }

  /**
   * Checks the connection to a host name on a port, and performs retries if necessary.
   * Returns a boolean true if connection exists.
   *
   * @description Does port connection check and returns a boolean.
   * @pronghornType method
   * @name checkPortConnection
   * @summary Check if connection to host on port exists, with retries
   * @param {string} hostName - e.g. google.com
   * @variableTooltip {hostName} e.g. google.com
   * @param {number} port - e.g. 80
   * @variableTooltip {port} e.g. 80
   * @param {number} [retries=5]
   * @variableTooltip {retries} e.g. 5
   * @param {number} [timeout=1000]
   * @variableTooltip {timeout} e.g. 5000
   * @param callback
   * @return {object} portConnection Result port connection test
   * @route {POST} /checkPortConnection
   * @roles admin
   * @task true
   * @example
   * {
    *    "hostName": "google.com",
    *    "port": 80,
    *    "retries": 5,
    *    "timeout": 1000
    * }
   */
  checkPortConnection(hostName, port, retries, timeout, callback) {
    let retriesNumber = retries;
    let timeoutDuration = timeout;
    if (!retriesNumber) {
      retriesNumber = 5;
    }
    if (!timeoutDuration) {
      timeoutDuration = 1000;
    }
    if (hostName === '' || typeof hostName !== 'string') {
      return callback(null, 'You must specify hostName as a string.');
    }
    if (!port || typeof port !== 'number') {
      return callback(null, 'You must specify port as a number.');
    }
    if ((retriesNumber && typeof retriesNumber !== 'number') || retriesNumber <= 0 || retriesNumber % 1 !== 0) {
      return callback(null, 'You must specify retries as a positive integer greater than 0.');
    }
    if (!timeoutDuration || typeof timeoutDuration !== 'number' || timeoutDuration < 0 || timeoutDuration > 10000) {
      return callback(null, 'You must specify timeout as a number greater than 0 and less than 10000.');
    }
    const tryConnect = (attempt) => {
      const client = new net.Socket();
      client.on('connect', () => {
        client.destroy();
        return callback(true, null);
      });
      client.on('timeout', () => {
        // attempt += 1;
        client.destroy();
        if (attempt < retriesNumber) {
          return tryConnect(1 + attempt);
        }
        return callback(false, null);
      });
      client.on('error', () => {
        client.destroy();
        if (attempt < retriesNumber) {
          return tryConnect(1 + attempt);
        }
        return callback(false, null);
      });
      client.setTimeout(timeoutDuration);
      return client.connect(port, hostName);
    };
    return tryConnect(1);
  }

  /**
   * Checks the connection to a list of host names and ports, and performs retries if necessary.
   * Returns an array of objects containing the original data and checkPortConnection
   * and checkPortConnectionMessge.
   *
   * @description Synchronously does port connection check and returns a boolean
   * @pronghornType method
   * @name checkPortConnectionList
   * @summary Synchronously check if connection to hosts on ports exist, with retries
   * @param {array} deviceArray
   * @variableTooltip {deviceArray} e.g. Array of objects containing name and port.
   * @param {number} [retries=1]
   * @variableTooltip {retries} e.g. 5
   * @param {number} [timeout=1000]
   * @variableTooltip {timeout} e.g. 5000
   * @param callback
   * @return {array} portConnectionList Result port connection test
   * @route {POST} /checkPortConnectionList
   * @roles admin
   * @task true
   * @example
   * {
   *    "deviceArray": [{ "name": "google.com", "port": 80 }],
   *    "retries": 5,
   *    "timeout": 5000
   * }
   */
  async checkPortConnectionList(deviceArray, retries, timeout, callback) {
    if (!deviceArray || !Array.isArray(deviceArray)) {
      return callback(null, 'checkPortConnectionList: deviceArray must be an array');
    }
    let retriesNumber = retries;
    let timeoutDuration = timeout;
    if (!retriesNumber) {
      retriesNumber = 5;
    }
    if (!timeoutDuration) {
      timeoutDuration = 1000;
    }
    const checkPortConnectionPromisified = itentialPromisify(this.checkPortConnection);
    const result = [];
    for (let i = 0; i < deviceArray.length; i += 1) {
      const device = deviceArray[i];
      const hostName = device.name;
      const { port } = device;
      device.checkPortConnection = false;
      device.checkPortConnectionMessage = `Could not connect to host via port ${port}`;
      try {
        // eslint-disable-next-line no-await-in-loop, max-len
        const status = await checkPortConnectionPromisified(hostName, parseInt(port, 10), retriesNumber, timeoutDuration);
        if (status === true) {
          device.checkPortConnection = true;
          device.checkPortConnectionMessage = `Connected to port ${port} successfully`;
        }
        result.push(device);
      } catch (err) {
        device.checkPortConnectionMessage = err;
        result.push(device);
      }
    }
    return callback(result);
  }

  /**
   * This method takes a URL string, parses it, and it will return a URL object
   * with each part of the address as properties
   *
   * @description This method takes a URL string, parses it, and it will return
   * a URL object with each part of the address as properties
   * @pronghornType method
   * @name parseUrl
   * @summary  Parse URL string to return different parts of URL
   * @param {string} urlString - e.g. http://example.com
   * @variableTooltip {urlString} e.g. http://example.com
   * @param callback
   * @return {object} response Parsed URL object
   * @roles admin
   * @task true
   * @route {POST} /parseUrl
   * @example
   * {
   *    "urlString": "http://example.com"
   * }
   */
  parseUrl(urlString, callback) {
    if (typeof urlString !== 'string' || urlString === '') {
      return callback(null, 'You must specify urlString as string.');
    }
    try {
      const urlObject = {};
      // Properties of URL object that are returned to the user.
      // Authentication password is not included.
      const keys = ['href', 'origin', 'protocol', 'username', 'host', 'hostname', 'port', 'pathname', 'search', 'hash'];
      const urlFields = new URL(urlString);
      keys.forEach((element) => {
        urlObject[element] = urlFields[element];
      });
      log.debug(`urlObject with necessary URL fields ${urlObject}`);
      return callback(urlObject, null);
    } catch (err) {
      log.error(err);
      return callback(null, err);
    }
  }

  /**
   * This method gets the IP address of the IAP server the task is running on
   *
   * @description This method gets the IP address of the IAP server the task is running on
   * @pronghornType method
   * @name getIPAddresOfCurrentIAPServer
   * @summary  Gets the IP address of the IAP server the task is running on
   * @param callback
   * @return {object} IP addresss of the IAP server the task is running on
   * @roles admin
   * @task true
   * @route {GET} /getIPAddresOfCurrentIAPServer
   */
  getIPAddresOfCurrentIAPServer(callback) {
    try {
      const nets = networkInterfaces();
      const results = Object.create(null); // Or just '{}', an empty object
      for (let netidx = 0; netidx < Object.keys(nets).length; netidx += 1) {
        const name = Object.keys(nets)[netidx];
        for (let netsidx = 0; netsidx < nets[name].length; netsidx += 1) {
          const network = nets[name][netsidx];
          // Skip over non-IPv4 and internal (i.e. 127.0.0.1) addresses
          // 'IPv4' is in Node <= 17, from 18 it's a number 4 or 6
          const familyV4Value = typeof network.family === 'string' ? 'IPv4' : 4;
          if (network.family === familyV4Value && !network.internal) {
            if (!results[name]) {
              results[name] = [];
            }
            results[name].push(network.address);
          }
        }
      }
      return callback(results, null);
    } catch (err) {
      log.error(err);
      return callback(null, err);
    }
  }

  /**
   * This method prints a given ENV variable
   *
   * @description This method prints a given ENV variable
   * @pronghornType method
   * @name printEnvVariable
   * @summary  printenv
   * @param {string} envVariable
   * @param callback
   * @return {object} response on printenv
   * @roles admin
   * @task true
   * @route {POST} /printEnvVariable
   */
  printEnvVariable(envVariable, callback) {
    if (typeof envVariable !== 'string' || envVariable === '') {
      return callback(null, 'You must specify envVariable as string.');
    }
    try {
      return callback(process.env[envVariable], null);
    } catch (err) {
      log.error(err);
      return callback(null, err);
    }
  }
}

module.exports = new NetworkTools();
