
## 1.2.5 [05-05-2023]

* Removed the npm run test pre-commit hook to fix the failing pipeline

See merge request itentialopensource/applications/app-network_tools!32

---

## 1.2.4 [12-06-2022]

* Removed ipTools capability

See merge request itentialopensource/applications/app-network_tools!26

---

## 1.2.3 [11-09-2022]

* Documented new methods in README, fixed pipeline

See merge request itentialopensource/applications/app-network_tools!25

---

## 1.2.2 [11-09-2022]

* Ip tools

See merge request itentialopensource/applications/app-network_tools!24

---

## 1.2.1 [10-31-2022]

* Update maintainers

See merge request itentialopensource/applications/app-network_tools!23

---

## 1.2.0 [09-21-2022]

* added GetIPAddresOfCurrentIAPServer and printEnvVariable tasks

See merge request itentialopensource/applications/app-network_tools!22

---

## 1.2.0 [09-07-2022]

* Added  getIPAddresOfCurrentIAPServer and printEnvVariable

---
## 1.1.0 [07-12-2022]

* Minor/sp 847

See merge request itentialopensource/applications/app-network_tools!21

---

## 1.0.2 [01-11-2021]

* updated how to install section in README file

See merge request itentialopensource/applications/app-network_tools!19

---

## 1.0.1 [07-28-2020]

* Update MAINTAINERS, package version, and pipeline node version

See merge request itentialopensource/applications/app-network_tools!18

---

## 0.1.1 [12-23-2019]

* add IAP dependencies

See merge request itentialopensource/applications/app-network_tools!17

---

## 0.1.0 [12-17-2019]

* Minor/port connection list

See merge request itentialopensource/applications/app-network_tools!16

---

## 0.0.8 [11-11-2019]

* Update name of sample workflow in README.md

See merge request itentialopensource/applications/app-network_tools!12

---

## 0.0.7 [11-11-2019]

* Update name of sample workflow in README.md

See merge request itentialopensource/applications/app-network_tools!12

---

## 0.0.6 [11-07-2019]

* Change keyword from App to Application

See merge request itentialopensource/applications/app-network_tools!10

---

## 0.0.5 [11-01-2019]

* change keyword from Application to App; ignore utils/phJson

See merge request itentialopensource/applications/app-network_tools!8

---

## 0.0.4 [11-01-2019]

* add expected keywords to package.json for App-Artifacts

See merge request itentialopensource/applications/app-network_tools!7

---

## 0.0.3 [11-01-2019]

* update image links to point to Gitlab public project in order to fix broken npmjs broken links

See merge request itentialopensource/applications/app-network_tools!6

---

## 0.0.2 [11-01-2019]

* Patch/dsup 765

See merge request itentialopensource/applications/app-network_tools!5

---
